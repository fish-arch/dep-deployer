#!/bin/bash

# check if environment file exists
FILE=config.json
if [ -f "$FILE" ]; then
  echo "$FILE exists"
else
  cp config.json.template config.json
  echo "Created config.json file"
fi