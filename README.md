# dep - Deployer

A **dep**loyer component for Fisherman architecture based on Deployer.

IMPORTANT: This deployer is intended to be used in a Ubuntu LTS release (at the time of writing 18.04).

# How to use

## 1. Set up deployer configuration

1.1. Please run the following command:

```bash
bash init.sh
```

This will copy the configurations template `config.json` into a non-tracked file (to avoid commiting credentials).

1.2 Edit the `config.json` and set up your configuration.

## 2. Install dependencies

Run the command:

```bash
bash install.sh # install PHP Deployer
dep install-deps # install deployment dependencies
```

## 3. Set up environment patches

The cloned/downloaded release to deploy, will be patched with the files inside the patch folder.

For example:

`app/environment.json (from repository root)`

Can be patched by placing a file at:

`patch/app/environment.json`

## 4. Deploy

Run the following command to deploy:

```bash
dep deploy
```

## TODOs:

- Add proper examples.
- Add proper documentation on the types supported.
- Add automatic tests.
- A lot of work.