<?php
namespace Deployer;

// TODO: Add install of nodejs + npm

function getProductionServiceData() {
  return [
    'ExecStart' => '{{release_path}}/api/njs-api',
    'WorkingDirectory' =>  '{{release_path}}/api/',
    'Environment' => array('PATH=/usr/bin:/usr/local/bin:/bin', 'NODE_ENV=production')
  ];
}

function getDevelopmentServiceData() {
  return [
    'ExecStart' => '/usr/bin/node {{release_path}}/api/bin/www',
    'WorkingDirectory' =>  '{{release_path}}/api/',
    'Environment' => array('PATH=/usr/bin:/usr/local/bin', 'NODE_ENV=production')
  ];
}
?>