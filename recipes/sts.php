<?php
namespace Deployer;

$dependenciesTask = 'sts:install-deps';
desc('Install deployment dependencies for STS');
task($dependenciesTask, 'sudo apt-get install -y zip unzip');
$GLOBALS['installDependenciesTasks'] =  array_merge($GLOBALS['installDependenciesTasks'], [$dependenciesTask]);

if (array_key_exists('deployToken', getConfigFile())) {
  desc('Prepare for install STS application .war');
  task('sts:prepare-install', function () {
    // TODO: Add support to use .war in /app folder
    if (test("[ -e {{release_path}}/app/sts.war ]")) {
      run('cd {{release_path}} && cp app/sts.war sts.war');
    }

    $applicationName = getConfigFile()['applicationName'];
    $testApplicationName = 'test-renamed';

    run('cd {{release_path}} && unzip -qq sts.war -d sts');
  });
  desc('Install STS application .war');
  task('sts:install', function () {
    $applicationName = getConfigFile()['applicationName'];
    $testApplicationName = 'test-renamed';

    // Replace context with test name
    // zip again as .war (test-depl)
    run('cd {{release_path}}/sts && sed -i -E \'s/(.*path="\\/)(.*)(">.*)/\\1' . $testApplicationName . '\\3/\' META-INF/context.xml');
    run("cd {{release_path}}/sts && zip -qq -r ../$testApplicationName.war *");

    // Replace context with app name
    // zip again as .war
    run('cd {{release_path}}/sts && sed -i -E \'s/(.*path="\\/)(.*)(">.*)/\\1' . $applicationName . '\\3/\' META-INF/context.xml');
    run("cd {{release_path}}/sts && zip -qq -r ../$applicationName.war *");

    // cleanup
    run('cd {{release_path}} && rm -r sts sts.war');

    // TEST:
    getConfigFile()['applicationName'] = $testApplicationName;
    invoke('tomcat:install-war');
    invoke('tomcat:test-war');
    run("cd {{release_path}} && rm $testApplicationName.war");
    invoke('tomcat:uninstall-war');

    getConfigFile()['applicationName'] = $applicationName;
    invoke('tomcat:install-war');
    invoke('tomcat:test-war');
    run("cd {{release_path}} && rm $applicationName.war");
  });
} else {
  // install SDK to build and install (not yet supported)
}

$GLOBALS['prepareTasks'] =  array_merge($GLOBALS['prepareTasks'], ['sts:prepare-install']);
$GLOBALS['deployTasks'] =  array_merge($GLOBALS['deployTasks'], ['sts:install']);
?>