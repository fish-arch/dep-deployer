<?php
namespace Deployer;

$dependenciesTask = 'db:install-deps';
desc('Install deployment dependencies for .NET Core');

task($dependenciesTask, 
  // install runtime
  'sudo apt-get -y install nodejs npm && ' .
  'sudo npm install -g sequelize sequelize-cli'
);

$GLOBALS['installDependenciesTasks'] =  array_merge($GLOBALS['installDependenciesTasks'], [$dependenciesTask]);

desc('Run migrations with the provided files');
task('db:migrations', function() {
  if (test('[ -e {{release_path}}/mig/natural-migrations.sh ]')) {
    run('cd {{release_path}}/mig && npm install && bash natural-migrations.sh');
  } else {
    writeln("Migration system not found?");
  }
});
?>