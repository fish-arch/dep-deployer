<?php
namespace Deployer;

$dependenciesTask = 'www:install-deps';
desc('Install deployment dependencies for web file operations');
task($dependenciesTask, '(command apk && apk add gzip brotli) || sudo apt-get -y install gzip brotli');
$GLOBALS['installDependenciesTasks'] =  array_merge($GLOBALS['installDependenciesTasks'], [$dependenciesTask]);

desc('Generate precompressed Gzip and Brotli archives for static files');
task('www:compress', "find . -type f -a \( -name '*.html' -o -name '*.css' -o -name '*.js' -o -name '*.json' -o -name '*.xml' -o -name '*.svg' -o -name '*.txt' \) -exec brotli --best {} \+ -exec gzip --best -k {} \+");
task('www:compress-web-app', function() {
  if (test("[ -e {{release_path}}/app ]")) {
    run("cd {{release_path}}/app && find . -type f -a \( -name '*.html' -o -name '*.css' -o -name '*.js' -o -name '*.json' -o -name '*.xml' -o -name '*.svg' -o -name '*.txt' \) -exec brotli --best {} \+ -exec gzip --best -k {} \+");
  }
});
?>