<?php
namespace Deployer;

task('webpack:build-app', function() { run("cd {{release_path}}/app && npm run build"); });
task('webpack:build-app-quick', function() { run("cd {{release_path}}/app && npm run build"); });
task('webpack:build-app-quick-dev', function() { run("cd {{release_path}}/app && npm run dev-build"); });
?>