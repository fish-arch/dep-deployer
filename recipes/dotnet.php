<?php
namespace Deployer;

// TODO: Add support for dotnet as app
$version = explode(":", array_key_exists('api', getConfigFile()) ? getConfigFile()['api'] : "")[1];

$dependenciesTask = 'dotnet:install-deps';
desc('Install deployment dependencies for .NET Core');
if (array_key_exists('deployToken', getConfigFile())) {
  task($dependenciesTask, 
    // install runtime
    'sudo apt-get -y install software-properties-common && ' .
    'wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb && ' .
    'sudo dpkg -i packages-microsoft-prod.deb && ' .
    'rm -f packages-microsoft-prod.deb && ' .
    'sudo add-apt-repository universe && ' .
    'sudo apt-get -y install apt-transport-https && ' .
    'sudo apt-get update && ' .
    'sudo apt-get -y install aspnetcore-runtime-' .$version
  );
} else {
  // install SDK
  task($dependenciesTask, 
    'sudo apt-get -y install software-properties-common && ' .
    'wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb && ' .
    'sudo dpkg -i packages-microsoft-prod.deb && ' .
    'rm -f packages-microsoft-prod.deb && ' .
    'sudo add-apt-repository universe && ' .
    'sudo apt-get -y install apt-transport-https && ' .
    'sudo apt-get update && ' .
    'sudo apt-get -y install dotnet-sdk-' .$version
  );
}
$GLOBALS['installDependenciesTasks'] =  array_merge($GLOBALS['installDependenciesTasks'], [$dependenciesTask]);

desc('Build NET.Core API on release mode');
task('dotnet:publish', 'dotnet publish -c Release --output bin/');
desc('Restore NET.Core API packages');
task('dotnet:restore', 'sudo dotnet restore');

task('dotnet:self-link', 'stat {{release_path}}/' . (array_key_exists('apiBuildPath', getConfigFile()) ? getConfigFile()['apiBuildPath'] : '') . ' || ln -s . api');
task('dotnet:publish-api', 'cd {{release_path}}/' . (array_key_exists('apiBuildPath', getConfigFile()) ? getConfigFile()['apiBuildPath'] : '') . ' && dotnet publish -c Release --output {{release_path}}/api/bin/');
task('dotnet:restore-api', 'cd {{release_path}}/' . (array_key_exists('apiBuildPath', getConfigFile()) ? getConfigFile()['apiBuildPath'] : ''). ' && dotnet restore');

function getProductionServiceData() {
  return [
    'ExecStart' => '/usr/bin/dotnet {{release_path}}/api/' . getConfigFile()['apiBinaryPath'],
    'WorkingDirectory' =>  '{{release_path}}/api/',
    'Environment' => array('PATH=/usr/bin:/usr/local/bin')
  ];
}

function getDevelopmentServiceData() {
  return getProductionServiceData();
}
?>