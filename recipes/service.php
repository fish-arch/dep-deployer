<?php
namespace Deployer;

function getServiceConfig() {
  $defaultConfig = array(
    'type' => 'node', // options: node, dotnet
    'mode' => 'development' // (UNUSED) options: development, production
  );

  $config = get('service', []);

  $config = array_merge($defaultConfig, $config);
  return $config;
}

desc('Install service for production install (built program if possible, no source code/debugging)');
task('service:install', "sudo tee /etc/systemd/system/{{application}}.service << EOL\n" . getServiceFile(getProductionServiceData()) . " \nEOL\n");
desc('Install service for development install (source code program for debugging if possible)');
task('service:install-dev', "sudo tee /etc/systemd/system/{{application}}.service << EOL\n" . getServiceFile(getDevelopmentServiceData()) . "\nEOL\n");
desc('Enable service autostart');
task('service:enable', "sudo systemctl daemon-reload && sudo systemctl enable {{application}}");
desc('Restart service');
task('service:restart', "sudo service {{application}} restart");
desc('Disable service autostart and stop it');
task('service:disable', "((sudo systemctl disable {{application}} || echo 'Failed to disable service?') && (sudo service {{application}} stop || echo 'Failed to stop service?') && sudo rm -f /etc/systemd/system/{{application}}.service && sudo systemctl daemon-reload) || echo 'First time installing the service? (service did not exist)'");
desc('Validate the executing user for the service exists');
task('service:validate-executing-user', "getent passwd " . getConfigFile()["executingUser"] . " > /dev/null || echo 'The user [" . getConfigFile()["executingUser"] . "] does not exist' && getent passwd " . getConfigFile()["executingUser"] . " > /dev/null");



function getServiceFile($data) {
$data['Environment'] = implode(PHP_EOL . "Environment=", $data['Environment']);
$serviceFile = $file=<<<'EOD'
[Unit]
Description={{application}} service
After=network.target
StartLimitIntervalSec=0
[Service]
Type=simple
Restart=always
RestartSec=1
User={{executingUser}}

EOD;

$serviceFile.= implode(PHP_EOL, array_map(
  function ($v, $k) {
    return $k.'='.$v;
  },
  $data,
  array_keys($data)
));

$serviceFile.= $file=<<<'EOD'

[Install]
WantedBy=multi-user.target
EOD;

//$serviceFile = str_replace('{{application}}', getConfigFile()["applicationName"], $serviceFile);
//$serviceFile = str_replace('{{executableFilePath}}', getAPIBinaryName(), $serviceFile);
$serviceFile = str_replace('{{executingUser}}', getConfigFile()["executingUser"], $serviceFile);
return $serviceFile;
}

?>