<?php
namespace Deployer;

desc('Build Angular app on production mode, full optimization');
task('ng:build', function() { run("ng build --prod --build-optimizer --base-href=" . getConfigFile()["appBaseHref"],  ['timeout' => 1800]); });
desc('Build Angular app on production mode, not optimized');
task('ng:build-quick', function() { run("ng build --prod --base-href=" . getConfigFile()["appBaseHref"],  ['timeout' => 1800]); });
desc('Build Angular app on development mode, not optimized');
task('ng:build-quick-dev', function() { run("ng build --base-href=" . getConfigFile()["appBaseHref"],  ['timeout' => 1800]); });

task('ng:build-app', function() {
  within('{{release_path}}/app', function() { invoke('ng:build'); });
});
task('ng:build-quick-app', function() {
  within('{{release_path}}/app', function() { invoke('ng:build-quick'); });
});
task('ng:build-quick-dev-app', function() {
  within('{{release_path}}/app', function() { invoke('ng:build-quick-dev'); });
});
?>