<?php
namespace Deployer;

task('rm:mig', 'cd {{release_path}} && rm -r -f mig'); // delete migrations folder
task('rm:dep', 'cd {{release_path}} && rm -r -f dep .git .gitignore .gitlab-ci.yml && mv api .api && mv app .app && rm -r -f * && mv .api api && mv .app app && rm -r -f */.vscode && mv app/dist dist && rm -r -f app && mkdir app && mv dist app/'); // delete deployment folder and other stuff
?>