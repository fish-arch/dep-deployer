<?php
namespace Deployer;

function getAPIBinaryName() {
  $settings = null;
  // Load NADS settings
  if(file_exists('.build/api/package.json')) {
    $json = file_get_contents('.build/api/package.json');
    $settings = json_decode($json,true);
  } else {
    throw new Exception('NADS Deployer could not find API package name! (package.json)');
  }
  return $settings["name"];
}
?>