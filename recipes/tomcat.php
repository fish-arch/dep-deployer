<?php
namespace Deployer;

$dependenciesTask = 'tomcat:install-deps';
desc('Install deployment dependencies for Tomcat');
task($dependenciesTask, '(command apk && apk add curl) || sudo apt-get -y install curl');
$GLOBALS['installDependenciesTasks'] =  array_merge($GLOBALS['installDependenciesTasks'], [$dependenciesTask]);

desc('Install .war');
task('tomcat:install-war', function() {
  $workingDirectory = run('echo $(pwd)'); // This URI is lost after the first invoke
  cd($workingDirectory);
  $tomcatProtocol = explode("://", getConfigFile()['tomcatHost'])[0];
  $tomcatHost = explode("://", getConfigFile()['tomcatHost'])[1];
  $tomcatUser = getConfigFile()['tomcatUser'];
  $tomcatPassword = getConfigFile()['tomcatPassword'];
  $applicationName = getConfigFile()['applicationName'];
  run("curl --fail -sT '{{release_path}}/$applicationName.war' '$tomcatProtocol://$tomcatUser:$tomcatPassword@$tomcatHost/manager/text/deploy?path=/$applicationName&update=true' > /dev/null");
});

desc('Test .war');
task('tomcat:test-war', function() {
  $workingDirectory = run('echo $(pwd)'); // This URI is lost after the first invoke
  cd($workingDirectory);
  $tomcatProtocol = explode("://", getConfigFile()['tomcatHost'])[0];
  $tomcatHost = explode("://", getConfigFile()['tomcatHost'])[1];
  $tomcatUser = getConfigFile()['tomcatUser'];
  $tomcatPassword = getConfigFile()['tomcatPassword'];
  $applicationName = getConfigFile()['applicationName'];
  $applicationTestUrl = array_key_exists('applicationTestUrl', getConfigFile()) ? getConfigFile('applicationTestUrl') : "$tomcatProtocol://$tomcatHost/$applicationName";
  if (!test("curl --fail -sSL1 $applicationTestUrl > /dev/null")) {
    throw new \Exception("CURL test failed for URL $applicationTestUrl");
  }
});

desc('Uninstall .war');
task('tomcat:uninstall-war', function() {
  $workingDirectory = run('echo $(pwd)'); // This URI is lost after the first invoke
  cd($workingDirectory);
  $tomcatProtocol = explode("://", getConfigFile()['tomcatHost'])[0];
  $tomcatHost = explode("://", getConfigFile()['tomcatHost'])[1];
  $tomcatUser = getConfigFile()['tomcatUser'];
  $tomcatPassword = getConfigFile()['tomcatPassword'];
  $applicationName = getConfigFile()['applicationName'];
  run("curl --fail -s '$tomcatProtocol://$tomcatUser:$tomcatPassword@$tomcatHost/manager/text/undeploy?path=/$applicationName' > /dev/null");
});
?>