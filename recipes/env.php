<?php
namespace Deployer;
task('env:set-files', function () {
  $stage = 'patch';

  if(has('roles') && get('roles')) {
    $stage = get('roles');
  }

  if(test("[ -e " . __DIR__ . "/../$stage ]")) {
    if(test("[ -e " . __DIR__ . "/../$stage/* ]")) {
      writeln('Local override settings detected, may be replaced by repository settings');
      run("cp -R " . __DIR__ . "/../$stage/* {{release_path}}");
    } else {
      writeln("No $stage environment files detected at " . __DIR__ . "/../$stage");
    }
  }
  else {
    if (test("[ -e {{release_path}}/dep/$stage ]")) {
      if (test("[ -e {{release_path}}/dep/$stage/* ]")) {
        writeln("Applying environment values for $stage...");
        run("cp -R {{release_path}}/dep/$stage/* {{release_path}}");
      } else {
        writeln("No $stage environment files detected at {{release_path}}/dep/$stage");
      }
    } else {
      throw new \Exception("ERROR: Environment settings for $stage not found!");
    }
  }

  if (array_key_exists('sed', getConfigFile())) {
    writeln('"sed" replacements detected, performing...');
    foreach(getConfigFile()["sed"] as $sedArguments) {
      run("cd {{release_path}} && sed $sedArguments");
    }
  }
});
?>