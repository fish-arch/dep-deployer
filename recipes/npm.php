<?php
namespace Deployer;

desc('Copy packages from old release');
task('npm:install-quick', function() { run('cp -L shared-package-lock.json package-lock.json && mkdir node_modules && rsync -r --links shared_node_modules/. node_modules/ --stats --include ".*" --include "..*"'); });
desc('Prepare to copy packages from previous release');
task('npm:install-prepare', function() { run('cp -L shared-package-lock.json package-lock.json && mkdir node_modules && rsync -r --links shared_node_modules/. node_modules/ --stats --include ".*" --include "..*"'); });
desc('Run npm install');
task('npm:install', function() { run('npm install'); });
desc('Finish copying packages from previous release');
task('npm:install-complete', function() { run('cp -H package-lock.json shared-package-lock.json && rsync -r --links node_modules/. shared_node_modules/'); });
desc('Perform clean packages install');
task('npm:install-full', function() {
  $workingDirectory = run('echo $(pwd)'); // This URI is lost after the first invoke
  cd($workingDirectory);
  if (test('[ -e shared-package-lock.json ]')) {
    cd($workingDirectory);
    invoke('npm:install-prepare');
  } else {
    writeln('<comment>No shared node modules install detected, using a standalone node modules install...</comment>');
  }
  cd($workingDirectory);
  invoke('npm:install');
  cd($workingDirectory);
  if (test('[ -e shared-package-lock.json ]')) {
    cd($workingDirectory);
    invoke('npm:install-complete');
  } else {
    writeln('<comment>Standalone node modules install completed.</comment>');
  }
});

task('npm:install-full-api', function() {
  within('{{release_path}}/api', function() { invoke('npm:install-full'); });
});
task('npm:install-full-app', function() {
  within('{{release_path}}/app', function() { invoke('npm:install-full');  });
});
task('npm:install-full-mig', function() {
  within('{{release_path}}/mig', function() { invoke('npm:install-full'); });
});

task('npm:install-quick-api', function() {
  within('{{release_path}}/api', function() { invoke('npm:install-quick'); });
});
task('npm:install-quick-app', function() {
  within('{{release_path}}/app', function() { invoke('npm:install-quick'); });
});
task('npm:install-quick-mig', function() {
  within('{{release_path}}/mig', function() { invoke('npm:install-quick'); });
});
?>