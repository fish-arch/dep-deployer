<?php
namespace Deployer;

$GLOBALS['configFile'] = null;

function getConfigFile() {
  if ($GLOBALS['configFile'] == null) {
  $settings = null;
  $configFile = file_exists('./config.json') ? './config.json' : './config.json.template';
  // Load deployer settings
  if(file_exists($configFile)) {
    $json = file_get_contents($configFile);
    $settings = json_decode($json,true);
  } else {
    throw new Exception("Deployer config file doesn't exist! ($configFile)");
  }
  $GLOBALS['configFile'] = $settings;
  } else {
    $settings = $GLOBALS['configFile'];
  }
  return $settings;
}

getConfigFile(); // Test getting settings
?>