<?php
namespace Deployer;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

option('job-id', null, InputOption::VALUE_REQUIRED, 'GitLab Job ID to fetch artifacts');
option('job-name', null, InputOption::VALUE_REQUIRED, 'GitLab Job name to fetch artifacts');

$dependenciesTask = 'gitlab:install-deps';
desc('Install deployment dependencies for GitLab');
task($dependenciesTask, '(command apk && apk add unzip) || sudo apt-get -y install unzip');
$GLOBALS['installDependenciesTasks'] =  array_merge($GLOBALS['installDependenciesTasks'], [$dependenciesTask]);

desc('Download prebuilt artifact from GitLab');
task('gitlab:download-artifact', function() {
  $jobIds = null;

  if(input()->hasOption('job-id') && input()->getOption('job-id')) {
    $jobIds  = explode(',',input()->getOption('job-id'));
  }

  if ($jobIds != null) {
    foreach($jobIds as $jobId) {
      run('cd {{release_path}} && curl -sL --header "PRIVATE-TOKEN:' . getConfigFile()["deployToken"] . '" "https://gitlab.com/api/v4/projects/'
      . str_replace('/', '%2F', 
          explode(".git", explode(":", getConfigFile()["repositoryUrl"])[1])[0]
        )
      . '/jobs/' . $jobId . '/artifacts" -o "artifacts.zip"');
      writeln('Job ID: ' . $jobId . ' Artifact MD5: ' . run('cd {{release_path}} && md5sum artifacts.zip'));
      invoke('gitlab:unzip-artifact');
    }
  } else {
    foreach(getConfigFile()["buildArtifacts"] as $jobName) { 
      run('cd {{release_path}} && curl -sL --header "PRIVATE-TOKEN:' . getConfigFile()["deployToken"] . '" "https://gitlab.com/api/v4/projects/'
      . str_replace('/', '%2F', 
          explode(".git", explode(":", getConfigFile()["repositoryUrl"])[1])[0]
        )
      . '/jobs/artifacts/{{branch}}/download?job=' . $jobName . '" -o "artifacts.zip"');
      writeln('Job Name: ' . $jobName . ' Artifact MD5: ' . run('cd {{release_path}} && md5sum artifacts.zip'));
      invoke('gitlab:unzip-artifact');
    }
  }
});
desc('Unzips prebuilt artifact from GitLab');
task('gitlab:unzip-artifact', function() {
  $repositoryName = explode(".git",
    array_values(array_slice(
      explode("/", getConfigFile()["repositoryUrl"])
      , -1))[0]
  )[0]; 

  run('cd {{release_path}} && mkdir .artifacts');
  run('cd {{release_path}} && unzip -o artifacts.zip -d .artifacts');
  run('cd {{release_path}} && rm artifacts.zip');
  
  for ($i = 0; $i < 2; ++$i) {
    // if there is a CI_JOB_ID (used to track CI_JOB_IDs during stages), erase it
    if (test('[ -e "{{release_path}}/.artifacts/CI_JOB_ID" ]')) {
      run('rm -r {{release_path}}/.artifacts/CI_JOB_ID');
    }
    
    // if there is a BUILD_JOB_ID (used to track BUILD_JOB_IDs during stages), erase it
    if (test('[ -e "{{release_path}}/.artifacts/BUILD_JOB_ID" ]')) {
      run('rm -r {{release_path}}/.artifacts/BUILD_JOB_ID');
    }

    // if there is a MIG_JOB_ID (used to track MIG_JOB_IDs during stages), erase it
    if (test('[ -e "{{release_path}}/.artifacts/MIG_JOB_ID" ]')) {
      run('rm -r {{release_path}}/.artifacts/MIG_JOB_ID');
    }

    // if there is a folder with the repository name, get into it (e.g. MyModule/api at MyModule.git)
    if (test('[ -e "{{release_path}}/.artifacts/' . $repositoryName . '" ]')) {
      run('mv "{{release_path}}/.artifacts/' . $repositoryName . '"/* {{release_path}}/.artifacts/');
      run('rm -r "{{release_path}}/.artifacts/' . $repositoryName . '"');
    }

    // if there is a build folder within the zip, we delete it
    if (test('[ -e "{{release_path}}/.artifacts/build" ]')) {
      run('mv {{release_path}}/.artifacts/build/* {{release_path}}/.artifacts/');
      run('rm -r {{release_path}}/.artifacts/build');
    }
  }

  // move artifacts to target now
  run('mv {{release_path}}/.artifacts/* {{release_path}}/');

  run('rm -r {{release_path}}/.artifacts');
});
?>