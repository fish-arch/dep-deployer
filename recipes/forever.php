<?php
namespace Deployer;

desc('Start node app using forever');
task('forever:start', function() {
  if (test('[ -f {{release_path}}/api/bin/www ]')) {
    run('cd {{release_path}}/api/ && forever start bin/www');
  } else {
    throw new \Exception("ERROR: {{release_path}}/api/bin/www is missing!");
  }
});
desc('Stop node app using forever');
task('forever:stop', function() {
  if (has('previous_release') && test('[ -e {{previous_release}}]')) {
    if(test('[ -f {{previous_release}}/api/bin/www ]')) {
      if (test('cd {{previous_release}}/api/ && forever stop bin/www')) {
        writeln("<info>Forever process $result has been stopped</info>");
      }
      else {
        writeln("<info>Running forever process not found (first time install?)</info>");
      }
    } else {
      writeln("<error>WARNING: {{previous_release}}/api/bin/www is missing! (damaged previous install?</error>");
    }
  }
  else {
    writeln("<comment>Previous deployment install not found (first time install?)</comment>");
  }
});

task('forever:start-api', ['forever:start']);
task('forever:stop-api', ['forever:stop']);
?>