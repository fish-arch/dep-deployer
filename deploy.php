<?php
namespace Deployer;

$GLOBALS['installDependenciesTasks'] = ['prepare-install-deps'];
$GLOBALS['prepareTasks'] = [];
$GLOBALS['deployTasks'] = [];
$GLOBALS['hasService'] = false;

require 'recipe/common.php';
require './recipes/config.php';
require './recipes/env.php';
require './recipes/gitlab.php'; // should be only required IF there is a deploy token
require './recipes/www.php'; // should be only required IF there is a web app
require './recipes/rm.php';

// TODO: Add app deployment procedure for app
if (array_key_exists('api', getConfigFile())) {
  if (strpos(getConfigFile()['api'], 'dotnet:') === 0) {
    require './recipes/dotnet.php';
  }
  else if (strpos(getConfigFile()['api'],('node:')) === 0) {
    require './recipes/node.php';
    require './recipes/npm.php';
  }
  require './recipes/service.php';
  $GLOBALS['hasService'] = true;
}
if (array_key_exists('app', getConfigFile())) {
  if (strpos(getConfigFile()["app"],('sts:')) === 0) {
    require './recipes/tomcat.php';
    require './recipes/sts.php';
  }
  if (strpos(getConfigFile()['app'],('ng:')) === 0) {
    require './recipes/ng.php';
  }
}
if (array_key_exists('mig', getConfigFile())) {
  if (strpos(getConfigFile()['mig'],('sequelize:')) === 0) {
    require './recipes/sequelize.php';
  }
  $GLOBALS['hasMigrations'] = true;
}

// Project name
set('application', getConfigFile()["applicationName"]);

// Project repository
set('repository', getConfigFile()["repositoryUrl"]);

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
set('shared_files', array_merge([], getConfigFile()["sharedFiles"] ?? []));
set('shared_dirs', array_merge([], getConfigFile()["sharedDirs"] ?? []));

// Writable dirs by web server
set('writable_dirs', []);
set('allow_anonymous_stats', false);

// Hosts
localhost()
    ->stage('development')
    ->set('deploy_path', '~/{{application}}');
  
// Default stage
set('default_stage', 'development');

// Tasks
desc('Align as API (moves all repository content to api/ folder if it does not exist)');
task('this:align-as-api', 'stat {{release_path}}/api || mkdir .api && mv * .api && mv .api api');
desc('Align as APP (moves all repository content to app/ folder if it does not exist)');
task('this:align-as-app', 'stat {{release_path}}/app || mkdir .app && mv * .app && mv .app app');
desc('Align as MIG (moves all repository content to mig/ folder if it does not exist)');
task('this:align-as-mig', 'stat {{release_path}}/mig || mkdir .mig && mv * .mig && mv .mig mig');

desc('Align project to deployer mode');

$componentsPresentCount = 
  (array_key_exists('api', getConfigFile()) ? 1 : 0) +
  (array_key_exists('app', getConfigFile()) ? 1 : 0) +
  (array_key_exists('mig', getConfigFile()) ? 1 : 0) +
  (array_key_exists('dep', getConfigFile()) ? 1 : 0)
  ;

  if ($componentsPresentCount == 1) {
    if (array_key_exists('api', getConfigFile())) {
      task('this:align', ['this:align-as-api']);
    } else if (array_key_exists('app', getConfigFile())) {
      task('this:align', ['this:align-as-app']);
    } else if (array_key_exists('mig', getConfigFile())) {
      task('this:align', ['this:align-as-mig']);
    }
  }
if (array_key_exists('api', getConfigFile())) {
  task('this:align', ['this:align-as-api']);
}

desc('Start deployer dependencies install');
task('prepare-install-deps', function() {
  writeln('Preparing to install deployer dependencies');
  invoke('deploy:info');
  invoke('deploy:prepare');
  invoke('deploy:lock');
  invoke('deploy:release');
});

$GLOBALS['installDependenciesTasks'] = array_merge($GLOBALS['installDependenciesTasks'], ['finish-install-deps']);
desc('Install deployer dependencies');
task('install-deps', function() {
  foreach ($GLOBALS['installDependenciesTasks'] as $task) {
    invoke($task);
  }
});

desc('Finish deployer dependencies install');
task('finish-install-deps', function() {
  invoke('deploy:unlock');
  writeln('Finished deployer dependencies installation');
});

if (array_key_exists("deployToken", getConfigFile())) {
  // Download and deploy
  desc('Deployment/Installation/Build prepare operations');
  task('this:prepare', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
  ]);
  desc('Deployment/Installation/Build complete operations');
  task('this:complete', [
    'deploy:shared',
    'deploy:writable',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
  ]);
  task('deploy',
    array_merge(
      [
        'this:prepare',
        'gitlab:download-artifact',
        'this:align',
      ],
      $GLOBALS['prepareTasks'],
      [
        'env:set-files',
        'www:compress-web-app',
      ],
      !array_key_exists('hasMigrations', $GLOBALS) || !$GLOBALS['hasMigrations'] ? [] : 
      [
        'db:migrations',
        'rm:mig',
      ],
      $GLOBALS['deployTasks'],
      !array_key_exists('hasService', $GLOBALS) || !$GLOBALS['hasService'] ? [] : 
      [
        'service:validate-executing-user', // hasService
        'service:disable', // hasService
        'service:install', // hasService
        'service:enable', // hasService
        'service:restart', // hasService
      ],
      [
        'this:complete'
      ]
    )
  );
} else {
  // Build and deploy
  desc('Deployment/Installation/Build prepare operations');
  task('this:prepare', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
  ]);
  desc('Deployment/Installation/Build complete operations');
  task('this:complete', [
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
  ]);
  desc('Deploy project');
  task('deploy', [
    'this:prepare',
    'dotnet:restore-api',
    'npm:install-full-mig',
    'env:set-files',
    'dotnet:publish-api',
    'db:natural-migrations',
    'service:validate-executing-user',
    'service:disable',
    'rm:api-dotnet-clean',
    'rm:mig',
    'rm:dep',
    'service:install-dev',
    'service:enable',
    'service:restart',
    'this:complete'
  ]);
}

task('remove', function() {
  set('deploy_path', '~/{{application}}');
  if ($GLOBALS['hasService']) {
    invoke('service:disable');
  }
});

task('uninstall', function() {
  invoke('remove');
});

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');