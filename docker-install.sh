#!/bin/bash
# CI/CD dependencies only

# Install Deployer
apt-get update -qq
apt-get install -y php-fpm curl
curl -LO https://deployer.org/deployer.phar
mv deployer.phar /usr/local/bin/dep
chmod +x /usr/local/bin/dep